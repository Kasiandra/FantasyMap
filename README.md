**Fantasy Map**

Demo: http://etroll.crux.uberspace.de/map/

This is a simple map created with [leaflet](https://leafletjs.com/) + [leaflet-search](https://github.com/stefanocudini/leaflet-search) + [jQuery](https://jquery.com/).

To change the map background, replace **img/map.jpg**. 

Set in **src/map.js** in [line 20](https://git.elektroll.art/Elektroll/FantasyMap/src/branch/master/src/map.js#L20) the resolution of your imagefile.
```
	var bounds = [xy(0, 0), xy(2480, 1754)];
```
Markers are set in **src/poi.js**
just add a new line behind `var data = [` with:
```
	{"loc":[y,x], "title":"Cityname"},
```
Replace **y** with the Y coordinates of your image and do the same with **x**. Replace **Cityname** with the Name of the place.
