
	var map = new L.Map('map', {crs: L.CRS.Simple, minZoom: -2, maxZoom: 1,center: new L.latLng(data[0].loc) });

	map.on('zoomend', function() {

	    var newzoom = '' + (20*(map.getZoom()+2)) +'px';
	    $('#map .Marker').css({'width':newzoom,'height':newzoom,'background-size':newzoom + ' ' + newzoom});

	});

	var yx = L.latLng;

	var xy = function(x, y) {
	  if (L.Util.isArray(x)) {
	    return yx(x[1], x[0]);
	  }
	  return yx(y, x);
	};

	var bounds = [xy(0, 0), xy(2480, 1754)];
	var image = L.imageOverlay('img/map.jpg', bounds).addTo(map);
	map.setView(xy(1206,473), 1);

	var markersLayer = new L.LayerGroup();

	map.addLayer(markersLayer);
//
var cityIcon = L.icon({
    iconUrl: 'img/marker-icon.png',
    iconSize: [48, 48],
    iconAnchor: [18, 24],
    popupAnchor: [0, -24],
		className: 'Marker'
});

//


	var controlSearch = new L.Control.Search({
		position:'topleft',
		hideMarkerOnCollapse: true,
		marker: {
			icon: false,
			circle: {
				radius: 20,
				color: '#ff0000',
				opacity: 1}},
		layer: markersLayer,
		initial: true,
		zoom: 2,
		autoResize: true,
		textPlaceholder: 'Ortsname',
	});

	map.addControl( controlSearch );

	for(i in data) {
		var title = data[i].title,
			loc = data[i].loc,
			marker = new L.Marker(new L.latLng(loc), {icon: cityIcon, title: title});
				marker.bindPopup('City: '+ title);
		markersLayer.addLayer(marker);
	};
